import pytest
import requests


class TestsPostsListingAllPosts:
    @pytest.fixture()
    def post_to_filter_1(self, test_data):
        yield test_data["posts to filter"][0]

    @pytest.fixture()
    def post_to_filter_2(self, test_data):
        yield test_data["posts to filter"][1]

    @pytest.fixture(scope="session")
    def get_all_posts_request(self, entrypoint):
        yield requests.get(f"{entrypoint}/posts")

    @pytest.fixture(scope="session")
    def get_all_posts_request_body(self, get_all_posts_request):
        yield get_all_posts_request.json()

    def test_status_code_is_correct(self, get_all_posts_request):
        assert get_all_posts_request.status_code == 200

    @pytest.mark.parametrize("post_to_filter", ["post_to_filter_1", "post_to_filter_2"])
    def test_post_in_all_posts(self, request, post_to_filter, get_all_posts_request_body):
        post_to_filter = request.getfixturevalue(post_to_filter)
        assert post_to_filter in get_all_posts_request_body
