import pytest
import requests


class TestsPostsCreatingAPost:
    @pytest.fixture(scope="session")
    def post_to_create(self, test_data):
        yield test_data["post to create"]

    @pytest.fixture(scope="session")
    def create_a_post_request(self, entrypoint, post_to_create):
        yield requests.post(f"{entrypoint}/posts", data=post_to_create)

    @pytest.fixture(scope="session")
    def create_a_post_request_body(self, create_a_post_request):
        yield create_a_post_request.json()

    @pytest.fixture(scope="session")
    def get_created_post_request(self, entrypoint, create_a_post_request_body):
        post_object_id = create_a_post_request_body["id"]
        yield requests.get(f"{entrypoint}/posts/{post_object_id}")

    @pytest.fixture(scope="session")
    def get_created_post_request_body(self, get_created_post_request):
        yield get_created_post_request.json()

    def test_status_code_is_correct(self, create_a_post_request):
        assert create_a_post_request.status_code == 201

    def test_body_user_id_is_correct(self, post_to_create, create_a_post_request_body):
        assert int(create_a_post_request_body["userId"]) == post_to_create["userId"]

    def test_body_title_is_correct(self, post_to_create, create_a_post_request_body):
        assert create_a_post_request_body["title"] == post_to_create["title"]

    def test_body_body_is_correct(self, post_to_create, create_a_post_request_body):
        assert create_a_post_request_body["body"] == post_to_create["body"]

    def test_get_status_code_is_correct(self, get_created_post_request):
        assert get_created_post_request.status_code == 200

    def test_get_body_id_is_correct(self, post_to_create, create_a_post_request_body, get_created_post_request_body):
        assert get_created_post_request_body["id"] == create_a_post_request_body["id"]

    def test_get_body_user_id_is_correct(self, post_to_create, get_created_post_request_body):
        assert get_created_post_request_body["userId"] == post_to_create["userId"]

    def test_get_body_title_is_correct(self, post_to_create, get_created_post_request_body):
        assert get_created_post_request_body["title"] == post_to_create["title"]

    def test_get_body_body_is_correct(self, post_to_create, get_created_post_request_body):
        assert get_created_post_request_body["body"] == post_to_create["body"]
