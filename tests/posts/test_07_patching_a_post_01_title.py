import pytest
import requests


class TestsPostsPatchingAPostTitle:
    @pytest.fixture(scope="session")
    def post_to_patch_title(self, test_data):
        yield test_data["post to patch title"]

    @pytest.fixture(scope="session")
    def post_patch_title(self, test_data):
        yield test_data["post patch title"]

    @pytest.fixture(scope="session")
    def patch_a_post_request(self, entrypoint, post_to_patch_title, post_patch_title):
        post_object_id = post_to_patch_title["id"]
        yield requests.patch(f"{entrypoint}/posts/{post_object_id}", data=post_patch_title)

    @pytest.fixture(scope="session")
    def patch_a_post_request_body(self, patch_a_post_request):
        yield patch_a_post_request.json()

    @pytest.fixture(scope="session")
    def get_patched_post_request(self, entrypoint, post_to_patch_title, patch_a_post_request_body):
        post_object_id = post_to_patch_title["id"]
        yield requests.get(f"{entrypoint}/posts/{post_object_id}")

    @pytest.fixture(scope="session")
    def get_patched_post_request_body(self, get_patched_post_request):
        yield get_patched_post_request.json()

    def test_status_code_is_correct(self, patch_a_post_request):
        assert patch_a_post_request.status_code == 200

    def test_body_id_is_correct(self, post_to_patch_title, patch_a_post_request_body):
        assert patch_a_post_request_body["id"] == post_to_patch_title["id"]

    def test_body_user_id_is_correct(self, post_to_patch_title, patch_a_post_request_body):
        assert int(patch_a_post_request_body["userId"]) == post_to_patch_title["userId"]

    def test_body_title_is_correct(self, post_patch_title, patch_a_post_request_body):
        assert patch_a_post_request_body["title"] == post_patch_title["title"]

    def test_body_body_is_correct(self, post_to_patch_title, patch_a_post_request_body):
        assert patch_a_post_request_body["body"] == post_to_patch_title["body"]

    def test_get_status_code_is_correct(self, get_patched_post_request):
        assert get_patched_post_request.status_code == 200

    def test_get_body_id_is_correct(self, post_to_patch_title, get_patched_post_request_body):
        assert get_patched_post_request_body["id"] == post_to_patch_title["id"]

    def test_get_body_user_id_is_correct(self, post_to_patch_title, get_patched_post_request_body):
        assert get_patched_post_request_body["userId"] == post_to_patch_title["userId"]

    def test_get_body_title_is_correct(self, post_patch_title, get_patched_post_request_body):
        assert get_patched_post_request_body["title"] == post_patch_title["title"]

    def test_get_body_body_is_correct(self, post_to_patch_title, get_patched_post_request_body):
        assert get_patched_post_request_body["body"] == post_to_patch_title["body"]
