import pytest
import requests


class TestsPostsGettingAPost:
    @pytest.fixture(scope="session")
    def post_to_get(self, test_data):
        yield test_data["post to get"]

    @pytest.fixture(scope="session")
    def get_a_post_request(self, entrypoint, post_to_get):
        post_object_id = post_to_get["id"]
        yield requests.get(f"{entrypoint}/posts/{post_object_id}")

    @pytest.fixture(scope="session")
    def get_a_post_request_body(self, get_a_post_request):
        yield get_a_post_request.json()

    def test_status_code_is_correct(self, get_a_post_request):
        assert get_a_post_request.status_code == 200

    def test_body_id_is_correct(self, post_to_get, get_a_post_request_body):
        assert get_a_post_request_body["id"] == post_to_get["id"]

    def test_body_user_id_is_correct(self, post_to_get, get_a_post_request_body):
        assert get_a_post_request_body["userId"] == post_to_get["userId"]

    def test_body_title_is_correct(self, post_to_get, get_a_post_request_body):
        assert get_a_post_request_body["title"] == post_to_get["title"]

    def test_body_body_is_correct(self, post_to_get, get_a_post_request_body):
        assert get_a_post_request_body["body"] == post_to_get["body"]
