import pytest
import requests


class TestsPostsUpdatingAPost:
    @pytest.fixture(scope="session")
    def post_to_update(self, test_data):
        yield test_data["post to update"]

    @pytest.fixture(scope="session")
    def update_a_post_request(self, entrypoint, post_to_update):
        post_object_id = post_to_update["id"]
        yield requests.put(f"{entrypoint}/posts/{post_object_id}", data=post_to_update)

    @pytest.fixture(scope="session")
    def update_a_post_request_body(self, update_a_post_request):
        yield update_a_post_request.json()

    @pytest.fixture(scope="session")
    def get_updated_post_request(self, entrypoint, post_to_update, update_a_post_request_body):
        post_object_id = post_to_update["id"]
        yield requests.get(f"{entrypoint}/posts/{post_object_id}")

    @pytest.fixture(scope="session")
    def get_updated_post_request_body(self, get_updated_post_request):
        yield get_updated_post_request.json()

    def test_status_code_is_correct(self, update_a_post_request):
        assert update_a_post_request.status_code == 200

    def test_body_id_is_correct(self, post_to_update, update_a_post_request_body):
        assert update_a_post_request_body["id"] == post_to_update["id"]

    def test_body_user_id_is_correct(self, post_to_update, update_a_post_request_body):
        assert int(update_a_post_request_body["userId"]) == post_to_update["userId"]

    def test_body_title_is_correct(self, post_to_update, update_a_post_request_body):
        assert update_a_post_request_body["title"] == post_to_update["title"]

    def test_body_body_is_correct(self, post_to_update, update_a_post_request_body):
        assert update_a_post_request_body["body"] == post_to_update["body"]

    def test_get_status_code_is_correct(self, test_data, get_updated_post_request):
        assert get_updated_post_request.status_code == 200

    def test_get_body_id_is_correct(self, post_to_update, get_updated_post_request_body):
        assert get_updated_post_request_body["id"] == post_to_update["id"]

    def test_get_body_user_id_is_correct(self, post_to_update, get_updated_post_request_body):
        assert get_updated_post_request_body["userId"] == post_to_update["userId"]

    def test_get_body_title_is_correct(self, post_to_update, get_updated_post_request_body):
        assert get_updated_post_request_body["title"] == post_to_update["title"]

    def test_get_body_body_is_correct(self, post_to_update, get_updated_post_request_body):
        assert get_updated_post_request_body["body"] == post_to_update["body"]
