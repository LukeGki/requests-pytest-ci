import pytest
import requests


class TestsPostsFilteringPosts:
    @pytest.fixture(scope="session")
    def get_filtered_posts_request(self, entrypoint, test_data):
        post_object_user_id = test_data["posts to filter"][0]["userId"]
        yield requests.get(f"{entrypoint}/posts?userId={post_object_user_id}")

    @pytest.fixture(scope="session")
    def get_filtered_posts_request_body(self, get_filtered_posts_request):
        yield get_filtered_posts_request.json()

    def test_status_code_is_correct(self, get_filtered_posts_request):
        assert get_filtered_posts_request.status_code == 200

    def test_number_of_posts_is_correct(self, get_filtered_posts_request_body):
        assert len(get_filtered_posts_request_body) == 10

    def test_post_in_filtered_posts(self, test_data, get_filtered_posts_request_body):
        post_object = test_data["posts to filter"][0]
        assert post_object in get_filtered_posts_request_body

    def test_post_not_in_filtered_posts(self, test_data, get_filtered_posts_request_body):
        post_object = test_data["posts to filter"][1]
        assert post_object not in get_filtered_posts_request_body
