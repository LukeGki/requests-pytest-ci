import pytest
import requests


class TestsPostsDeletingAPost:
    @pytest.fixture(scope="session")
    def post_to_delete(self, test_data):
        yield test_data["post to delete"]

    @pytest.fixture(scope="session")
    def delete_a_post_request(self, entrypoint, post_to_delete):
        post_object_id = post_to_delete["id"]
        yield requests.delete(f"{entrypoint}/posts/{post_object_id}")

    @pytest.fixture(scope="session")
    def delete_a_post_request_body(self, delete_a_post_request):
        yield delete_a_post_request.json()

    @pytest.fixture(scope="session")
    def get_deleted_post_request(self, entrypoint, post_to_delete, delete_a_post_request_body):
        post_object_id = post_to_delete["id"]
        yield requests.get(f"{entrypoint}/posts/{post_object_id}")

    @pytest.fixture(scope="session")
    def get_deleted_post_request_body(self, get_deleted_post_request):
        yield get_deleted_post_request.json()

    def test_status_code_is_correct(self, delete_a_post_request):
        assert delete_a_post_request.status_code == 200

    def test_body_is_correct(self, delete_a_post_request_body):
        assert delete_a_post_request_body == {}

    def test_get_status_code_is_correct(self, get_deleted_post_request):
        assert get_deleted_post_request.status_code == 404

    def test_get_body_is_correct(self, get_deleted_post_request_body):
        assert get_deleted_post_request_body == {}
