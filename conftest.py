import os
import json

import pytest


@pytest.fixture(scope="session")
def entrypoint():
    with open(os.path.join("test_data", "env.json")) as f:
        env_data = json.loads(f.read())
    entrypoint = env_data["entrypoint"]

    yield entrypoint


@pytest.fixture(scope="session")
def test_data():
    with open(os.path.join("test_data", "posts.json")) as f:
        test_data = json.loads(f.read())

    yield test_data
